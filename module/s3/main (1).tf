resource "aws_cloudformation_stack" "s3test" {
  name = "${var.bucket_prefix}-${var.bucket}"
  capabilities = ["CAPABILITY_AUTO_EXPAND"]
  template_body = file("${path.module}/s3_policy.yml")
  # parameters we need to add
  parameters = {
    BucketName = var.BucketName
    KMSKeyId = var.KMSKeyId
    AccessControl= var.AccessControl
    VersioningConfiguration = var.VersioningConfiguration 
    OwnershipControls= var.OwnershipControls
    DestinationBucketName = var.DestinationBucketName
    AWSAccountId = var.AWSAccountId
    # tags for bucket
    BusinessApp = var.BusinessApp
    USquad = var.USquad
    Upciasset  = var.Upciasset
    Ubusinessowner = var.Ubusinessowner
    Uchargebackgroup  = var.Uchargebackgroup
    Uterraform = var.Uterraform
    Urepo = var.Urepo
    
    }
}
    


