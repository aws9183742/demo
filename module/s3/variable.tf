# stack variables 
variable "AWSAccountId" {}
variable "aws_region" {}
variable "bucket_prefix" {}
variable "bucket" {}
# Bucket property variable
variable "BucketName" {}
variable "KMSKeyId" {}
variable "AccessControl" {}
variable "VersioningConfiguration"{}
variable "OwnershipControls"{}
variable "DestinationBucketName"{}
# bucket tag variable
variable "BusinessApp" {}
variable "USquad" {}
variable "Upciasset" {}
variable "Ubusinessowner" {}
variable "Uchargebackgroup" {}
variable "Uterraform" {}
variable "Urepo" {}






