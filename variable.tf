variable "aws_account_id" {
  type = number
  default = "371273931689"
}

variable "aws_region" {
  type = string
  default = "us-east-1"
}

variable "bucket_name" {
  type = string
  default = "ala-a371273931689-sys-test14-us-east-1"
}

# stack name variables start
variable "bucket_prefix" {
  type        = string
  default     = "stack"
}

variable "bucket" {
  type        = string
  default     = "75b879aa95839006a"
}
# stack name variables end

# default tags start
variable "u_business_app" {
  type = string
  default = "automation_cicd"
}
variable "u_squad" {
  type = string
  default = "cloud_ops"
}
variable "u_pci_asset" {
  type = string
  default = "false"
}
variable "u_business_owner" {
  type = string
  default = "ala-aws-cloudops@alphaluna.com"
}
variable "u_chargeback_group" {
  type = string
  default = "cloud_ops"
}
variable "u_terraform" {
  type = string
  default = "false"
}
variable "u_repo" {
  type = string
  default = "none"
}
# default tags end

variable "accesscontrol" {
  type    = string
  default = "Private"
}

variable "blockpublicaccess" {
  type    = string
  default = "true"
}

variable "enforcebucketownerfullcontrol" {
  type    = string
  default = "true"
}

variable "kmskeyid" {
  type    = string
  default = "arn:aws:kms:us-east-1:371273931689:key/46e2290f-8ce8-4c90-adee-1333c8d57689"
}

variable "serversideencryption" {
  type    = string
  default = "KmsManagedKeys"
}

variable "versioning" {
  type    = string
  default = "Enabled"
}
